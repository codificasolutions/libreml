#!/bin/bash

# Runs jupyter notebooks and python scripts contained within this folder
# Intended to be run from within the libreML docker container for integration testing

set -eu -o pipefail

for f in jupyter/*; do
    echo "-------- Running ${f} -------"
    jupyter nbconvert --to notebook --execute ${f}
    echo "-----------------------------"
done

for f in python/*; do 
    echo "-------- Running ${f} -------"
    python3 ${f}
    echo "-----------------------------"
done
