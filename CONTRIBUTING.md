# Contributing

Thank you very much for wanting to contribute to libreML. Here are some guidelines to ensure that your patches can be merged with minimal fuss into libreML master.

With thanks to [freedesktop-SDK's](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/master/CONTRIBUTING.md) excellent documentation, for which this contributing guide is based on.

Here are the steps in order to submit a patch to libreML:

1. [Create a GitLab account](#create-a-gitlab-account)
2. [Clone our GitLab repo](#clone-our-gitlab-repo)
3. [Make a local branch](#make-a-local-branch)
4. [Make your changes in the branch](#make-your-changes-in-the-branch)
5. [Request developer access to the libreML repo](#request-developer-access-to-the-libreML-repo)
6. [Push your changes to the remote](#push-your-changes-to-the-remote)
7. [Open a merge request](#open-a-merge-request)

## Help and contact

You can find us at #libreml on freenode

## Create a Gitlab account

Simply click "sign in/register" in the top right corner and fill in the form.

Open a terminal and type:
```
git clone https://gitlab.com/libreml/libreml.git
```
If you have an SSH key associated with your GitLab account you can alternatively type
```
git clone git@gitlab.com:libreml/libreml.git
```
to avoid having to type your password when pushing.

Please make sure that your git is connected with your email and name using
```
git config user.name "MY_NAME"
git config user.email "MY_EMAIL@example.com"
```

There are also many GUI git interfaces, such as integration with IDEs and GitHub's UI, however running through all of these alternative methods would be a mammoth task. Feel free to add instructions for your personal preferred git workflow.

## Make a local branch
Make sure you're within the `libreML` directory and use the command
```
git checkout master
```

Now run

```
git checkout -b gitlab_username/my-branch-name
```
Your branch should be named something suitable for the changes you will be making. We also usually include our GitLab username in the branch name as `gitlab_username/suitable-branch-name`

## Make your changes in the branch
Create the necessary changes to fix your bug/add your feature.

When writing commit messages follow these rules:
1. Separate subject from body with a blank line - This makes your commit easier to read
2. Limit the subject line to 50 characters - Subject lines should be short and to the point
3. Capitalise the subject line - This is stylistic, but makes things clearer
4. Do not end the subject line with a period - Trailing punctuation is an unnecessary waste of space
5. Use the imperative case in the subject line i.e. As an instruction (e.g. Clean up x.bst) - This makes it clearer what your patch does
6. Wrap the body at 72 characters - This makes reading your messages easier for people with small displays
7. Use the body to explain what and why, rather than how - When looking through the git history at changes it may not be obvious *why* a change was implemented, making code impossible to maintain

For more information on git commit messages see [this guide](https://chris.beams.io/posts/git-commit/#seven-rules/).

## When modifying a git source
We use tools to track versions/security vulnerabilities, this requires us to
track the exact version number we are using.

Freedesktop-sdk does not utilise a traditional sha format, we combine
the version number, number of commits from the tag(HEAD) and then the sha,
which is easier to parse and automate.

When modifying a "ref" (bst sha) for a git source, you need to generate a new
ref format for the new version, this is possible by running:

```
git describe --tags --long --abbrev=40

```

Which should produce something like:

```
freedesktop-sdk-18.08.18-116-g30eb35057d1e2b0beb539b92d3af6708c252d21b

```

This ref clearly states the project version in a human readable format, whilst
also making it easier to regex/automate the parsing of the version by other
tools.

We also have CI to automatically track the latest tags of git repos, but this
requires use of the `git_tag` plugin rather than `git`. If adding a new git
source please use `git_tag` rather than `git`.


## Request developer access to the libreML repo
Go to our GitLab [project page](https://gitlab.com/libreml/libreml) and click the "Request Developer Access" button near the top of the page. One of the maintainers will review your request. Developer access allows you to push directly to our repo, enabling a simpler "push and merge request" workflow instead of using the GitHub "fork and pull request" workflow.
This has the added benefit of allowing you to use our CI, which is equipped with runners and will ultimately allow you to push your changes to libreML cache.

## Push your changes to the remote
Run the following commands:

```
git push --set-upstream origin my-branch-name
```
Your branch will now begin going through our CI runners, which build the SDK on multiple architectures to ensure that your changes have not caused a break anywhere. Additionally it will check for ABI changes.

It is also good practice to rebase your changes before pushing (so that your commits are on top of the target branch), to do this run
```
git rebase origin/master
```
while your branch is checked out. This may require you to force-push your branch, to do this add the `-f` switch to the git push command.

## Open a merge request
Navigate to the [New Merge Request Page](https://gitlab.com/libreml/libreml/merge_requests/new) on our GitLab, add your branch as the source branch and 19.08 as the target branch.
Once your MR is open it will be reviewed before merging. Once it passes our CI and any discussions are resolved, it will be assigned to our merge bot, which will continually rebase onto the latest commit in 19.08 until it merges your branch.

Congratulations, you are now a libreML contributor!

## Testing locally
If you want to test your changes locally then you will need to first install [BuildStream](https://buildstream.build). The installation instructions can be found [here](https://buildstream.build/install.html). Note that we use the latest stable version of BuildStream, so ensure you use this version too (otherwise you may not hit our cache server, and have to build everything from scratch). At time of writing, we use BuildStream 1.4.1.

We also use some plugins from the [bst-external](https://gitlab.com/BuildStream/bst-external) repository. To install these run the following commands:
```
git clone https://gitlab.com/BuildStream/bst-external
pip3 install --user -e ./bst-external
```
Again, we use the latest stable version of bst-external, which is usually the
latest release.

Once you have those installed you can build libreML!

#### Create the libreML sysroot

##### Dependencies

- [Buildstream](https://buildstream.build/install.html) (1.4.1 release)
- [bst-external](https://gitlab.com/BuildStream/bst-external) (>=0.7.0 required)
- Docker (Tested on 18.09.0)
- Linux OS (Tested on Debian Stretch). 

```shell
bst build libreml.bst
bst checkout libreml.bst <OUTPUT_DIR>
```

#### Create the docker container
To run the docker containers you can use:

```shell
docker import <OUTPUT_DIR>/libreml.tar.gz libreml:latest
docker run -p 8888:8888 registry.gitlab.com/libreml/libreml/libreml:<TAG> jupyter-notebook --ip=0.0.0.0 --port=8888 --allow-root
```