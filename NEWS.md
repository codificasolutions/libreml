# LibreML

## v0.0.2

- Add process for tracking and updating elements (#42)
- Choose between different BLAS architectures (#7)
    - cBLAS, ATLAS (No CI build) and OpenBLAS available
- Add support for testing key elements during libreML builds (#15)
- Add OpenCV (#37)
- Add PyTorch (#38)
- Update Freedesktop-sdk to 19.0X (#79)
- Support for ARM builds (#84)
- Added contributing guide (#32)
- Rename the libreml project to libreML
- Updates to all elements (as of 21/10/19)

With thanks to: Thomas Coldrick, Richard Dale, Javier Jardón, Adam Jones and Tom Mewett, without whose contributions this release would not be possible!

## v0.0.1b

- Fix critical openblas build issue (#43)
    - Build was tied to host machine build, rather than for multiple architectures.

## v0.0.1

- Setup Libreml
- Use freedesktop-sdk as the base runtime (#2)
- Use buildstream as the core tool for the project. 
- Added core data-science packages
    - scikit-learn (#10)
    - numpy (#8), scipy (#9), pandas (#16)
    - matplotlib (#18)
    - jupyter notebook (#17)
- Package libreml as a sysroot tarball (#20), (#23) and docker container (#26).
- Utitlise cache server from libreml (#6)
    - Also see https://gitlab.com/libreml/infrastructure/infrastructure

